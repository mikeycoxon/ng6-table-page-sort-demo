import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JediResolverService } from './_services/jedi.resolver.service';
import { JediComponent } from './jedi/jedi.component';

const routes: Routes = [
  { path: '', redirectTo: 'jedi', pathMatch: 'full' },
  { path: 'jedi', component: JediComponent, resolve: {jedi: JediResolverService} },
  // otherwise redirect to login
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'}) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
