import { Jedi } from './jedi';

export function createTestData(): Jedi[] {
  const jedi: Jedi[] = [];
  jedi.push(new Jedi({name: 'Mace Windu', race: 'human', alias: '',
    homeworld: 'Haruun Kal', status: 'baddass', master: 'Cyslin Myr', rank: 'Jedi Master'}));
  jedi.push(new Jedi({name: 'Luke Skywalker', race: 'human', alias: '',
    homeworld: 'Tatooine',  status: 'good', master: 'Yoda', rank: 'Jedi Knight'}));
  jedi.push(new Jedi({name: 'Obi Wan Kenobi', race: 'human', alias: 'Ben Kenobi',
    homeworld: 'Stewjon',  status: 'good', master: 'Qui-Gon Jinn', rank: 'Jedi Master'}));
  jedi.push(new Jedi({name: 'Yoda', race: 'unknown', alias: '',
    homeworld: 'unknown', status: 'good', master: 'Garro', rank: 'Jedi Grand Master'}));
  jedi.push(new Jedi({name: 'Maul', race: 'dathomirian', alias: 'Darth Maul',
    homeworld: 'Dathomir',  status: 'bad', master: 'Darth Sideous', rank: 'Sith Lord'}));
  jedi.push(new Jedi({name: 'Ezra Bridger', race: 'human', alias: '',
    homeworld: 'Lothal', status: 'good', master: 'Kanan Jarrus', rank: 'Jedi'}));
  jedi.push(new Jedi({name: 'Kanan Jarrus', race: 'human', alias: '',
    homeworld: 'Coruscant',  status: 'good', master: 'Depa Billaba', rank: 'Jedi'}));
  jedi.push(new Jedi({name: 'Qui-Gon Jinn', race: 'human', alias: '',
    homeworld: 'Coruscant',  status: 'good', master: 'Count Dooku', rank: 'Jedi Master'}));
  jedi.push(new Jedi({name: 'Count Dooku', race: 'human', alias: 'Darth Tyranus',
    homeworld: 'Serenno',  status: 'rich and bad', master: 'Yoda', rank: 'Jedi Master'}));
  jedi.push(new Jedi({name: 'Ben Solo', race: 'human', alias: 'Kylo Ren',
    homeworld: 'Chandrilla',  status: 'confused', master: 'Luke Skywalker', rank: 'Jedi'}));
  jedi.push(new Jedi({name: 'Luminara Unduli', race: 'mirialan', alias: '',
    homeworld: 'Mirial',  status: 'good', master: '', rank: 'Jedi Master'}));
  jedi.push(new Jedi({name: 'Barriss Offee', race: 'mirialan', alias: '',
    homeworld: 'Mirial',  status: 'naughty', master: 'Luminara Unduli', rank: 'Jedi'}));
  jedi.push(new Jedi({name: 'Snoke', race: 'alien', alias: '',
    homeworld: 'Unknown Regions',  status: 'bad', master: 'unknown', rank: 'Supreme Leader'}));
  jedi.push(new Jedi({name: 'Anakin Skywalker', race: 'human', alias: 'Darth Vader',
    homeworld: 'Tatooine',  status: 'insipid', master: 'Obi Wan Kenobi', rank: 'Sith Lord'}));
  jedi.push(new Jedi({name: 'Sheev Palpatine', race: 'human', alias: 'Darth Sidious',
    homeworld: 'Naboo',  status: 'really bad', master: 'Darth Plagueis', rank: 'Sith Lord'}));
  return jedi;
}
