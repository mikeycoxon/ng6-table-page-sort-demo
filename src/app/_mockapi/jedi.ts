export class Jedi implements IJedi {
  id: number;
  name: string;
  race: string;
  alias: string;
  homeworld: string;
  status: string;
  master: string;
  rank: string;

  constructor(obj: Jedi = {} as Jedi) {
    const {
      id = '',
      name = '',
      race = '',
      alias = '',
      homeworld = '',
      status = '',
      master = '',
      rank = ''
    } = obj;

    this.id = id;
    this.name = name;
    this.race = race;
    this.alias = alias;
    this.homeworld = homeworld;
    this.status = status;
    this.master = master;
    this.rank = rank;
  }

  equals(o: Jedi): boolean {
    if (this.name === o.name &&
      this.alias === o.alias &&
      this.homeworld === o.homeworld &&
      this.race === o.race &&
      this.status === o.status &&
      this.master === o.master &&
      this.rank === o.rank
    ) {
      return true;
    } else {
      return false;
    }
  }

  empty(): boolean {
    if (this.name === '' &&
      this.alias === '' &&
      this.homeworld === '' &&
      this.race === '' &&
      this.status === '' &&
      this.master === '' &&
      this.rank === ''
    ) {
      return true;
    } else {
      return false;
    }
  }
}

export interface IJedi {
  id: number;
  name: string;
  race: string;
  alias: string;
  homeworld: string;
  status: string;
  master: string;
  rank: string;
}
