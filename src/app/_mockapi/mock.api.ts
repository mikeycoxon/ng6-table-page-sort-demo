import {
  HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Jedi } from './jedi';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/materialize';
import 'rxjs/add/operator/dematerialize';
import 'rxjs/add/operator/delay';
import { createTestData } from './mock.data.seeder';

@Injectable()
export class MockApi implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return Observable.of(null).mergeMap(() => {

      if (request.url.endsWith('/api/jedi') && request.method === 'GET') {
        const jedi: Jedi[] = JSON.parse(localStorage.getItem('jedi'));
        return Observable.of(new HttpResponse({status: 200, body: jedi}));
      }

      if (request.url.match('/api/reset') && request.method === 'DELETE') {
        console.log('reset?');
        localStorage.clear();
        const jedi: Jedi[] = createTestData();
        localStorage.setItem('jedi', JSON.stringify(jedi));

        return Observable.of(new HttpResponse({status: 200, body: 'cleared'}));
      }

      // pass through any requests not handled above
      return next.handle(request);
    })

      .materialize()
      .delay(500)
      .dematerialize();
  }
}

export let fakeBackendProvider = {
  // use fake backend in place of Http service for backend-less development
  provide: HTTP_INTERCEPTORS,
  useClass: MockApi,
  multi: true
};

