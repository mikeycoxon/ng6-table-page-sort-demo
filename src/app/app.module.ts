import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { JediService } from './_services/jedi.service';
import { JediResolverService } from './_services/jedi.resolver.service';
import { fakeBackendProvider } from './_mockapi/mock.api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { JediComponent } from './jedi/jedi.component';

@NgModule({
  declarations: [
    AppComponent,
    JediComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    fakeBackendProvider,
    JediResolverService,
    JediService],
  bootstrap: [AppComponent]
})
export class AppModule { }
