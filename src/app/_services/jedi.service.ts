import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Jedi } from '../_mockapi/jedi';

@Injectable()
export class JediService {

  constructor(private http: HttpClient) { }

  getAll() {
    console.log('getAll() called');
    return this.http.get<Jedi[]>('/api/jedi');
  }

  clearTheBoards() {
    return this.http.delete<any>('/api/reset');
  }

  save(jedi: Jedi) {
    if (jedi.id) {
      return this.http.post('/api/jedi/' + jedi.id, jedi);
    } else {
      return this.http.post('/api/jedi', jedi);
    }
  }

  delete(id: number) {
    console.log('deleting user ' + id);
    return this.http.delete('api/jedi/' + id);
  }


}
