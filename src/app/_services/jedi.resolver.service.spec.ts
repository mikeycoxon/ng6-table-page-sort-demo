import { TestBed, inject } from '@angular/core/testing';

import { JediResolverService } from './jedi.resolver.service';

describe('JediResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JediResolverService]
    });
  });

  it('should be created', inject([JediResolverService], (service: JediResolverService) => {
    expect(service).toBeTruthy();
  }));
});
