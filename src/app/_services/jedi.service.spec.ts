import { TestBed, inject } from '@angular/core/testing';

import { JediService } from './jedi.service';

describe('JediService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JediService]
    });
  });

  it('should be created', inject([JediService], (service: JediService) => {
    expect(service).toBeTruthy();
  }));
});
