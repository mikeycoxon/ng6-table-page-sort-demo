import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { JediService } from './jedi.service';
import { Jedi } from '../_mockapi/jedi';

@Injectable()
export class JediResolverService implements Resolve<Observable<any>> {

  constructor(
    private service: JediService,
    private router: Router
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
     return this.service.getAll().pipe(
      catchError(err => {
        console.error(err); // deal with API error (eg not found)
        this.router.navigate(['/']); // could redirect to error page
        return Observable.create<Jedi[]>();
      }));
  }
}
