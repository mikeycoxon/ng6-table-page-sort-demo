import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JediService } from '../_services/jedi.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Jedi } from '../_mockapi/jedi';

@Component({
  selector: 'app-jedi',
  templateUrl: './jedi.component.html',
  styleUrls: ['./jedi.component.css']
})
export class JediComponent implements OnInit {

  data: any;
  f: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private jediService: JediService,
    private router: Router
  ) { }

  ngOnInit() {
    this.data = this.route.snapshot.data;
    console.log(this.route.snapshot.data);

    this.buildForm(this.data);
  }

  buildForm(data: Jedi) {
    this.f = this.fb.group({
      jedis: this.fb.array([]),
    });

    this.initJedis(data);

  }

  initJedis(data) {
    const control = <FormArray>this.f.controls['jedis'];
    if (data && data.jedi.length > 0) {
      this.data.jedi.forEach(x => {
        control.push(this.initJedi(x));
      });
    } else {
      control.push(this.initJedi());
    }
  }

  initJedi(jedi?: Jedi): FormGroup {
    jedi = jedi ? jedi : new Jedi();
    return this.fb.group({
      name: [jedi.name || '', Validators.required],
      race: [jedi.race || ''],
      alias: [jedi.alias || ''],
      homeworld: [jedi.homeworld || ''],
      status: [jedi.status || ''],
      master: [jedi.master || ''],
      rank: [jedi.rank || '']
    });
  }

  addJedi(jedi: Jedi) {
    const control = this.f.get('jedis') as FormArray;
    control.push(this.initJedi(jedi));
  }

  deleteJedi(i: number) {
    const control = this.f.get('jedis') as FormArray;
    if (i === 0) {
      control.setControl(i, this.initJedi());
      this.data.jedi.splice(i, 1, new Jedi());
    } else {
      control.removeAt(i);
      this.data.jedi.splice(i, 1);
    }
    this.jediService.delete(this.data.jedi.i)
  }

  saveJedi(i: number) {
    const formArray = this.f.get('jedis') as FormArray;
    let f_jedi: Jedi = formArray.controls[i].value as Jedi;

    let isDuplicate = false;
    let isAnEmpty = false;
    let matchedModelId: number;
    for (let j = 0; j < this.data.jedis.length; j++) {
      const m_jedi: Jedi = this.data.jedis[j];
      if (f_jedi.equals(m_jedi)) {
        isDuplicate = true;
        matchedModelId = m_jedi.id;
        break;
      } else if (f_jedi.empty()) {
        isAnEmpty = true;
        this.data.jedis.splice(j, 1, f_jedi);
        break;
      }
    }

    if (!isDuplicate) {
      if (!isAnEmpty) {
        f_jedi.id = matchedModelId;
        this.jediService.save(f_jedi);
      }
    }
  }

  resetData() {
    this.jediService.clearTheBoards().subscribe(
      data => {
        console.log(data);
        this.router.navigate(['/jedi']);
      }
    );

  }

}
